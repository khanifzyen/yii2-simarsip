<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefMasalah1 */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Ref Masalah1',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ref Masalah1s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-masalah1-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
