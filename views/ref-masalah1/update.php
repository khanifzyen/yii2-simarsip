<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RefMasalah1 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Ref Masalah1',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ref Masalah1s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ref-masalah1-update">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
