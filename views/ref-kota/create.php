<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefKota */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => ' Kota',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kota'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kota-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
