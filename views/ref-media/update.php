<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RefMedia */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Ref Media',
]) . ' ' . $model->int;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ref Media'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->int, 'url' => ['view', 'id' => $model->int]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ref-media-update">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
