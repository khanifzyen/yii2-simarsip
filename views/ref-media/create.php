<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefMedia */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Ref Media',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ref Media'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-media-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
