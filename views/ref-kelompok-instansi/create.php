<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefKelompokInstansi */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Ref Kelompok Instansi',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ref Kelompok Instansis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-kelompok-instansi-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
