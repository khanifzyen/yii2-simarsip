<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\NamaBerkas */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Nama Berkas',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nama Berkas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nama-berkas-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
