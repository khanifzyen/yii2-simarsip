<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\widgets\FileInput;
use kartik\widgets\SwitchInput;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\PelaksanaArsipInaktif */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pelaksana-arsip-inaktif-form">

    <?php     $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'options' => ['enctype' => 'multipart/form-data']   // important, needed for file upload
    ]);?>


            <div class="row">
        <div class="col-md-6">
        
            <?= $form->field($model, 'tahun_olah')->textInput() ?>

            <?= $form->field($model, 'pengolahan_ke')->textInput() ?>

            <?= $form->field($model, 'hasil_olahan')->textInput() ?>
        </div>

        <div class="col-md-6">
        
            <?= $form->field($model, 'id_ref_lembaga_pencipta')->textInput() ?>

            <?= $form->field($model, 'tahun_akuisisi')->textInput() ?>

            <?= $form->field($model, 'seri_dpa')->textInput(['maxlength' => 45]) ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

        <?php ActiveForm::end(); ?>

</div>

