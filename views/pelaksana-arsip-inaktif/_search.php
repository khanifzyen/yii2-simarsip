<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PelaksanaArsipInaktifSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pelaksana-arsip-inaktif-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_ref_lembaga_pencipta') ?>

    <?= $form->field($model, 'tahun_olah') ?>

    <?= $form->field($model, 'tahun_akuisisi') ?>

    <?= $form->field($model, 'pengolahan_ke') ?>

    <?php // echo $form->field($model, 'seri_dpa') ?>

    <?php // echo $form->field($model, 'hasil_olahan') ?>

    <?php // echo $form->field($model, 'userCreate') ?>

    <?php // echo $form->field($model, 'userUpdate') ?>

    <?php // echo $form->field($model, 'createDate') ?>

    <?php // echo $form->field($model, 'updateDate') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
