<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\PelaksanaArsipInaktif */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pelaksana Arsip Inaktifs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pelaksana-arsip-inaktif-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_ref_lembaga_pencipta',
            'tahun_olah',
            'tahun_akuisisi',
            'pengolahan_ke',
            'seri_dpa',
            'hasil_olahan',
             [
                'attribute' => 'userCreate',
                'value' => $model->userCreateLabel,
            ],
                    [
                'attribute' => 'userUpdate',
                'value' => $model->userUpdateLabel,
            ],
            
                    [
                'attribute' => 'createDate',
                'value' => $model->createDate,
            ],
            
                    [
                'attribute' => 'updateDate',
                'value' => $model->updateDate,
            ],
                
                ]]) ;?>

</div>
