<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\widgets\FileInput;
use kartik\widgets\SwitchInput;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Inaktif */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inaktif-form">

    <?php     $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'options' => ['enctype' => 'multipart/form-data']   // important, needed for file upload
    ]);?>


            <div class="row">
        <div class="col-md-6">
        
            <?= $form->field($model, 'kode_pelaksana')->textInput(['maxlength' => 45]) ?>

            <?= $form->field($model, 'id_klasifikasi')->textInput() ?>

            <?= $form->field($model, 'bulan1')->textInput() ?>

            <?= $form->field($model, 'bulan2')->textInput() ?>

            <?= $form->field($model, 'perlengkapan')->textInput() ?>

            <?= $form->field($model, 'media_id')->textInput() ?>

            <?= $form->field($model, 'tahun_aktif')->textInput() ?>

            <?= $form->field($model, 'tahun_retensi')->textInput(['maxlength' => 45]) ?>

            <?= $form->field($model, 'nilai_guna')->textInput() ?>
        </div>

        <div class="col-md-6">
        
            <?= $form->field($model, 'id_pelaksana_arsip_inaktif')->textInput() ?>

            <?= $form->field($model, 'no_fisis')->textInput() ?>

            <?= $form->field($model, 'isi_arsip')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'tahun1')->textInput() ?>

            <?= $form->field($model, 'tahun2')->textInput() ?>

            <?= $form->field($model, 'tk_perkembangan')->textInput() ?>

            <?= $form->field($model, 'id_jenis_series')->textInput() ?>

            <?= $form->field($model, 'tahun_inaktif')->textInput(['maxlength' => 45]) ?>

            <?= $form->field($model, 'ket_jra')->textInput() ?>

            <?= $form->field($model, 'id_user_pelaksana_arsip_inaktif')->textInput() ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

        <?php ActiveForm::end(); ?>

</div>

