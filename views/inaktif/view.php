<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Inaktif */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inaktifs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inaktif-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_pelaksana_arsip_inaktif',
            'kode_pelaksana',
            'no_fisis',
            'id_klasifikasi',
            'isi_arsip:ntext',
            'bulan1',
            'tahun1',
            'bulan2',
            'tahun2',
            'perlengkapan',
            'tk_perkembangan',
            'media_id',
            'id_jenis_series',
            'tahun_aktif',
            'tahun_inaktif',
            'tahun_retensi',
            'ket_jra',
            'nilai_guna',
            [
                'attribute' => 'createDate',
                'value' => $model->createDate,
            ],
            
                    [
                'attribute' => 'updateDate',
                'value' => $model->updateDate,
            ],
                
                     [
                'attribute' => 'userCreate',
                'value' => $model->userCreateLabel,
            ],
                    [
                'attribute' => 'userUpdate',
                'value' => $model->userUpdateLabel,
            ],
            
                    'id_user_pelaksana_arsip_inaktif',
        ]]) ;?>

</div>
