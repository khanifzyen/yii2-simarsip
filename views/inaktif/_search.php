<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InaktifSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inaktif-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_pelaksana_arsip_inaktif') ?>

    <?= $form->field($model, 'kode_pelaksana') ?>

    <?= $form->field($model, 'no_fisis') ?>

    <?= $form->field($model, 'id_klasifikasi') ?>

    <?php // echo $form->field($model, 'isi_arsip') ?>

    <?php // echo $form->field($model, 'bulan1') ?>

    <?php // echo $form->field($model, 'tahun1') ?>

    <?php // echo $form->field($model, 'bulan2') ?>

    <?php // echo $form->field($model, 'tahun2') ?>

    <?php // echo $form->field($model, 'perlengkapan') ?>

    <?php // echo $form->field($model, 'tk_perkembangan') ?>

    <?php // echo $form->field($model, 'media_id') ?>

    <?php // echo $form->field($model, 'id_jenis_series') ?>

    <?php // echo $form->field($model, 'tahun_aktif') ?>

    <?php // echo $form->field($model, 'tahun_inaktif') ?>

    <?php // echo $form->field($model, 'tahun_retensi') ?>

    <?php // echo $form->field($model, 'ket_jra') ?>

    <?php // echo $form->field($model, 'nilai_guna') ?>

    <?php // echo $form->field($model, 'createDate') ?>

    <?php // echo $form->field($model, 'updateDate') ?>

    <?php // echo $form->field($model, 'userCreate') ?>

    <?php // echo $form->field($model, 'userUpdate') ?>

    <?php // echo $form->field($model, 'id_user_pelaksana_arsip_inaktif') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
