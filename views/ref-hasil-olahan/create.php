<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefHasilOlahan */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Ref Hasil Olahan',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ref Hasil Olahans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-hasil-olahan-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
