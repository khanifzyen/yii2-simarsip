<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TempatPenyimpanan */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Tempat Penyimpanan',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tempat Penyimpanans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tempat-penyimpanan-update">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
