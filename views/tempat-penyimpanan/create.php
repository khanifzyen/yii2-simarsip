<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TempatPenyimpanan */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Tempat Penyimpanan',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tempat Penyimpanans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tempat-penyimpanan-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
