<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Arsip */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Arsip',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Arsips'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="arsip-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
