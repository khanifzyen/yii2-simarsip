<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ArsipSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="arsip-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'no_urut') ?>

    <?= $form->field($model, 'id_dari_kepada') ?>

    <?= $form->field($model, 'klasifikasi') ?>

    <?= $form->field($model, 'no_surat') ?>

    <?php // echo $form->field($model, 'id_nama_berkas') ?>

    <?php // echo $form->field($model, 'tgl_surat') ?>

    <?php // echo $form->field($model, 'tgl_terima') ?>

    <?php // echo $form->field($model, 'isi') ?>

    <?php // echo $form->field($model, 'raktif') ?>

    <?php // echo $form->field($model, 'rinaktif') ?>

    <?php // echo $form->field($model, 'tahun_raktif') ?>

    <?php // echo $form->field($model, 'tahun_rinaktif') ?>

    <?php // echo $form->field($model, 'id_ket_jra') ?>

    <?php // echo $form->field($model, 'id_nilai_guna') ?>

    <?php // echo $form->field($model, 'id_tempat_penyimpanan') ?>

    <?php // echo $form->field($model, 'id_tk_perkembangan') ?>

    <?php // echo $form->field($model, 'tgl_terus') ?>

    <?php // echo $form->field($model, 'id_unit_kerja') ?>

    <?php // echo $form->field($model, 'perihal') ?>

    <?php // echo $form->field($model, 'catatan_disposisi') ?>

    <?php // echo $form->field($model, 'id_sifat_surat') ?>

    <?php // echo $form->field($model, 'id_tindakan') ?>

    <?php // echo $form->field($model, 'id_jenis_arsip') ?>

    <?php // echo $form->field($model, 'scan_arsip') ?>

    <?php // echo $form->field($model, 'createDate') ?>

    <?php // echo $form->field($model, 'updateDate') ?>

    <?php // echo $form->field($model, 'userCreate') ?>

    <?php // echo $form->field($model, 'userUpdate') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
