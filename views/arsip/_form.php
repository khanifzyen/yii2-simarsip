<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\DaftarInstansi;
use app\models\RefNilaiGuna;
use app\models\RefTkPerkembangan;
use app\models\UnitKerja;
use app\models\RefTindakan;
use app\models\Klasifikasi;
use app\models\NamaBerkas;
use app\models\RefKetJra;
use app\models\TempatPenyimpanan;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\widgets\FileInput;
use kartik\widgets\SwitchInput;
use mihaildev\ckeditor\CKEditor;
use kartik\widgets\DatePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Arsip */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="arsip-form">

    <?php     $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'options' => ['enctype' => 'multipart/form-data']   // important, needed for file upload
    ]);?>


        <div class="row">
            <div class="col-md-12"><h4>Identitas Surat</h4><hr /></div>
        </div>
        <div class="row"> <!-- baris pertama: identitas surat-->
        
        <div class="col-md-6">
        
            
            <?php //echo $form->field($model, 'id_dari_kepada')->textInput()->label('Dari') ?>
            <?= $form->field($model, 'id_dari_kepada')->label('Dari')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(DaftarInstansi::find()->all(),'id','nama_instansi'),
                //'language' => 'de',
                'options' => ['placeholder' => 'Pilih Instansi/Personal...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>

            <div class="form-group">
                <?= Html::label('Wil / Kota','nama_kota',['class'=>'col-md-2 control-label']);?>
                    <div class="col-md-10">
                        <?= Html::textInput('nama_kota','',['id'=>'nama_kota','class'=>'form-control','disabled'=>'disabled']);?>
                    </div>
            </div>
            <?= $form->field($model, 'no_urut')->textInput() ?>
            <?= $form->field($model, 'no_surat')->textInput(['maxlength' => 255]) ?>
            <?=
            $form->field($model, "tgl_surat")->widget(DatePicker::classname(), [
                "options" => ["placeholder" => "Masukkan tanggal ..."],
                "pluginOptions" => [
                    "autoclose" => true,
                    "format" => "yyyy-mm-dd"
                ]
            ])
            ?>

            <?= $form->field($model, 'isi')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'rinaktif')->textInput() ?>

            <?= $form->field($model, 'tahun_rinaktif')->textInput() ?>

            <?php //echo $form->field($model, 'id_nilai_guna')->textInput() ?>
            <?= $form->field($model, 'id_nilai_guna')->label('Nilai Guna')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(RefNilaiGuna::find()->all(),'id','nama'),
                //'language' => 'de',
                'options' => ['placeholder' => 'Pilih Nilai Guna...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>

            <?php //echo $form->field($model, 'id_tk_perkembangan')->textInput() ?>
            <?= $form->field($model, 'id_tk_perkembangan')->label('Tk Perkembangan')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(RefTkPerkembangan::find()->all(),'id','nama'),
                //'language' => 'de',
                'options' => ['placeholder' => 'Pilih Tk Perkembangan...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>

            <?php // echo $form->field($model, 'id_unit_kerja')->textInput() ?>
            <?= $form->field($model, 'id_unit_kerja')->label('Diteruskan ke')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(UnitKerja::find()->all(),'id','nama'),
                //'language' => 'de',
                'options' => ['placeholder' => 'Pilih tujuan...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>

            <?= $form->field($model, 'catatan_disposisi')->textInput(['maxlength' => 255]) ?>

            <?php //echo $form->field($model, 'id_tindakan')->textInput() ?>
             <?= $form->field($model, 'id_tindakan')->label('Tindakan')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(RefTindakan::find()->all(),'id','nama'),
                //'language' => 'de',
                'options' => ['placeholder' => 'Pilih Tindakan...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>

            <?php //echo $form->field($model, 'scan_arsip')->textInput(['maxlength' => 255]) ?>
        </div>

        <div class="col-md-6">
        
           

            <?php // echo $form->field($model, 'klasifikasi')->textInput() ?>
             <?= $form->field($model, 'klasifikasi')->label('Klasifikasi')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Klasifikasi::find()->all(),'id','klasifikasi'),
                //'language' => 'de',
                'options' => ['placeholder' => 'Pilih Klasifikasi...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>

            <?php //echo $form->field($model, 'id_nama_berkas')->textInput() ?>
            <?= $form->field($model, 'id_nama_berkas')->label('Nama Berkas')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(NamaBerkas::find()->all(),'id','nama'),
                //'language' => 'de',
                'options' => ['placeholder' => 'Pilih Nama Berkas...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
            <?=
            $form->field($model, "tgl_terima")->widget(DatePicker::classname(), [
                "options" => ["placeholder" => "Masukkan tanggal ..."],
                "pluginOptions" => [
                    "autoclose" => true,
                    "format" => "yyyy-mm-dd"
                ]
            ])
            ?>
            <?= $form->field($model, 'raktif')->textInput() ?>

            <?= $form->field($model, 'tahun_raktif')->textInput() ?>

            <?php //echo $form->field($model, 'id_ket_jra')->textInput() ?>
            <?= $form->field($model, 'id_ket_jra')->label('Ket JRA')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(RefKetJra::find()->all(),'id','nama'),
                //'language' => 'de',
                'options' => ['placeholder' => 'Pilih JRA...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
            <?php //echo $form->field($model, 'id_tempat_penyimpanan')->textInput() ?>
            <?= $form->field($model, 'id_tempat_penyimpanan')->label('Tempat Penyimpanan')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(TempatPenyimpanan::find()->all(),'id','nama'),
                //'language' => 'de',
                'options' => ['placeholder' => 'Pilih Tempat Penyimpanan...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
            <?=
            $form->field($model, "tgl_terus")->widget(DatePicker::classname(), [
                "options" => ["placeholder" => "Masukkan tanggal ..."],
                "pluginOptions" => [
                    "autoclose" => true,
                    "format" => "yyyy-mm-dd"
                ]
            ])
            ?>
            <?= $form->field($model, 'perihal')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'id_sifat_surat')->textInput() ?>

            <?= $form->field($model, 'id_jenis_arsip')->textInput() ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

        <?php ActiveForm::end(); ?>

</div>

