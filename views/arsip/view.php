<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Arsip */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Arsips'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="arsip-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'no_urut',
            'id_dari_kepada',
            'klasifikasi',
            'no_surat',
            'id_nama_berkas',
            'tgl_surat',
            'tgl_terima',
            'isi',
            'raktif',
            'rinaktif',
            'tahun_raktif',
            'tahun_rinaktif',
            'id_ket_jra',
            'id_nilai_guna',
            'id_tempat_penyimpanan',
            'id_tk_perkembangan',
            'tgl_terus',
            'id_unit_kerja',
            'perihal',
            'catatan_disposisi',
            'id_sifat_surat',
            'id_tindakan',
            'id_jenis_arsip',
            'scan_arsip',
            [
                'attribute' => 'createDate',
                'value' => $model->createDate,
            ],
            
                    [
                'attribute' => 'updateDate',
                'value' => $model->updateDate,
            ],
                
                     [
                'attribute' => 'userCreate',
                'value' => $model->userCreateLabel,
            ],
                    [
                'attribute' => 'userUpdate',
                'value' => $model->userUpdateLabel,
            ],
            
                ]]) ;?>

</div>
