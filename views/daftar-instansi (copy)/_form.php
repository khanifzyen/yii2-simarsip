<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\widgets\FileInput;
use kartik\widgets\SwitchInput;
use kartik\select2\Select2;
use mihaildev\ckeditor\CKEditor;
use app\models\RefKota;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\DaftarInstansi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="daftar-instansi-form">

    <?php     $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'options' => ['enctype' => 'multipart/form-data']   // important, needed for file upload
    ]);?>


            <div class="row">
        <div class="col-md-6">

            <?= $form->field($model, 'nama_instansi')->textInput(['maxlength' => 255]) ?>
        
            <?= $form->field($model, 'alamat')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'nama_kota')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(RefKota::find()->all(),'id','nama'),
                //'language' => 'de',
                'options' => ['placeholder' => 'Pilih Kota...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>

              <div class="form-group">
                <div class="col-md-10 col-md-offset-2"><?= Html::a('<span class="glyphicon glyphicon-paperclip"> Tambah Kota</span>',Url::to(['ref-kota/create']),['class'=>'btn btn-primary btn-xs']);?>
                </div>
            </div>

            <?= $form->field($model, 'kelompok')->textInput(['maxlength' => 45]) ?>

             <?= $form->field($model, 'negara')->textInput(['maxlength' => 45]) ?>

            
        </div>

        <div class="col-md-6">
        
            <?= $form->field($model, 'telepon')->textInput(['maxlength' => 45]) ?>  

            <?= $form->field($model, 'kode_pos')->textInput(['maxlength' => 10]) ?>

            <?= $form->field($model, 'fax')->textInput(['maxlength' => 45]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => 100]) ?>

            <?= $form->field($model, 'website')->textInput(['maxlength' => 100]) ?>            

          

           


        </div>

    </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '<span class="glyphicon glyphicon-plus"> Create</span>') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

        <?php ActiveForm::end(); ?>

</div>

