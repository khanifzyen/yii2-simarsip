<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefLembagaPencipta */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Ref Lembaga Pencipta',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ref Lembaga Penciptas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-lembaga-pencipta-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
