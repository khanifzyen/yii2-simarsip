<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserPelaksanaArsipInaktif */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'User Pelaksana Arsip Inaktif',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Pelaksana Arsip Inaktifs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-pelaksana-arsip-inaktif-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
