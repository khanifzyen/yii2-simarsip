<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Klasifikasi */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Klasifikasi',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Klasifikasis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="klasifikasi-update">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
