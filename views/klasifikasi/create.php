<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Klasifikasi */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Klasifikasi',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Klasifikasis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="klasifikasi-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
