<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\widgets\FileInput;
use kartik\widgets\SwitchInput;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Klasifikasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="klasifikasi-form">

    <?php     $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'options' => ['enctype' => 'multipart/form-data']   // important, needed for file upload
    ]);?>


            <div class="row">
        <div class="col-md-6">
        
            <?= $form->field($model, 'klasifikasi1')->textInput(['maxlength' => 10]) ?>

            <?= $form->field($model, 'masalah1')->textInput() ?>

            <?= $form->field($model, 'sub_masalah')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'series')->textInput() ?>

            <?= $form->field($model, 'rinaktif')->textInput() ?>

            <?= $form->field($model, 'nilai_guna')->textInput() ?>
        </div>

        <div class="col-md-6">
        
            <?= $form->field($model, 'klasifikasi')->textInput(['maxlength' => 10]) ?>

            <?= $form->field($model, 'klasifikasi2')->textInput(['maxlength' => 10]) ?>

            <?= $form->field($model, 'masalah2')->textInput() ?>

            <?= $form->field($model, 'indeks')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'raktif')->textInput() ?>

            <?= $form->field($model, 'ket_jra')->textInput() ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

        <?php ActiveForm::end(); ?>

</div>

