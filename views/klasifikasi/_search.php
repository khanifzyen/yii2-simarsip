<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KlasifikasiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="klasifikasi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'klasifikasi') ?>

    <?= $form->field($model, 'klasifikasi1') ?>

    <?= $form->field($model, 'klasifikasi2') ?>

    <?= $form->field($model, 'masalah1') ?>

    <?php // echo $form->field($model, 'masalah2') ?>

    <?php // echo $form->field($model, 'sub_masalah') ?>

    <?php // echo $form->field($model, 'indeks') ?>

    <?php // echo $form->field($model, 'series') ?>

    <?php // echo $form->field($model, 'raktif') ?>

    <?php // echo $form->field($model, 'rinaktif') ?>

    <?php // echo $form->field($model, 'ket_jra') ?>

    <?php // echo $form->field($model, 'nilai_guna') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
