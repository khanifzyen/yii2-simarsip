<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefMasalah2 */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Ref Masalah2',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ref Masalah2s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-masalah2-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
