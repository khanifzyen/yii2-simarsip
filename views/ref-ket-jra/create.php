<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefKetJra */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Ref Ket Jra',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ref Ket Jras'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-ket-jra-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
