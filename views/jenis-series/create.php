<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\JenisSeries */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Jenis Series',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jenis Series'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-series-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
