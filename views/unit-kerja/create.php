<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UnitKerja */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Unit Kerja',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Unit Kerjas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-kerja-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
