<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefTindakan */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Ref Tindakan',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ref Tindakans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-tindakan-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
