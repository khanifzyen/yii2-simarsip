<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DaftarInstansi */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Daftar Instansi',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Daftar Instansi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daftar-instansi-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
