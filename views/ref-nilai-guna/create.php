<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefNilaiGuna */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Ref Nilai Guna',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ref Nilai Gunas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-nilai-guna-create">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
