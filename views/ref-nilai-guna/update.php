<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RefNilaiGuna */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Ref Nilai Guna',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ref Nilai Gunas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ref-nilai-guna-update">

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
