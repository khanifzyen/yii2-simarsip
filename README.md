##yii2-simarsip

Ini adalah aplikasi manajemen arsip berbasis yii2. Untuk download, disarankan menggunakan git

	git clone https://khanifzyen@bitbucket.org/khanifzyen/yii2-simarsip.git

Kemudian lakukan composer update

	composer update

Edit file config/web.php, ganti config db sesuai dengan db anda.

    'dsn' => 'mysql:host=localhost;dbname=dbarsip',
    'username' => 'root',
    'password' => '555555',

Lakukan migrasi

    ./yii migrate/up

Edit file config/web.php, pada key 'homeUrl' dan 'baseUrl' ganti valuenya sesuai dengan lokasi aplikasi anda terhadap posisi root webserver

    'homeUrl' => '/yii2/projects',
    ...
    'baseUrl' => '/yii2/projects', 

Jalankan di localhost

Selamat mencoba