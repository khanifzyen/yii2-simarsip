<?php

use yii\db\Schema;

class m150514_140101_create_tbl_ref_perlengkapan extends \yii\db\Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%tbl_ref_perlengkapan}}', [
            'id' => Schema::TYPE_PK,
            'nama' => Schema::TYPE_STRING . '(45) NOT NULL',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%tbl_ref_perlengkapan}}');
    }
}
