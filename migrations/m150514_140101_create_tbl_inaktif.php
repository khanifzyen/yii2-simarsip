<?php

use yii\db\Schema;

class m150514_140101_create_tbl_inaktif extends \yii\db\Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%tbl_inaktif}}', [
            'id' => Schema::TYPE_PK,
            'id_pelaksana_arsip_inaktif' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'kode_pelaksana' => Schema::TYPE_STRING . '(45)',
            'no_fisis' => Schema::TYPE_INTEGER . '(11)',
            'id_klasifikasi' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'isi_arsip' => Schema::TYPE_TEXT . ' NOT NULL',
            'bulan1' => Schema::TYPE_INTEGER . '(11)',
            'tahun1' => Schema::TYPE_INTEGER . '(11)',
            'bulan2' => Schema::TYPE_INTEGER . '(11)',
            'tahun2' => Schema::TYPE_INTEGER . '(11)',
            'perlengkapan' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'tk_perkembangan' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'media_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'id_jenis_series' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'tahun_aktif' => Schema::TYPE_INTEGER . '(11)',
            'tahun_inaktif' => Schema::TYPE_STRING . '(45)',
            'tahun_retensi' => Schema::TYPE_STRING . '(45)',
            'ket_jra' => Schema::TYPE_INTEGER . '(11)',
            'nilai_guna' => Schema::TYPE_INTEGER . '(11)',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
            'id_user_pelaksana_arsip_inaktif' => Schema::TYPE_INTEGER . '(11)',
            'FOREIGN KEY ([[id_pelaksana_arsip_inaktif]]) REFERENCES {{%tbl_pelaksana_arsip_inaktif}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[id_klasifikasi]]) REFERENCES {{%tbl_klasifikasi}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[perlengkapan]]) REFERENCES {{%tbl_ref_perlengkapan}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[tk_perkembangan]]) REFERENCES {{%tbl_ref_tk_perkembangan}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[media_id]]) REFERENCES {{%tbl_ref_media}} ([[int]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[id_jenis_series]]) REFERENCES {{%tbl_jenis_series}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[ket_jra]]) REFERENCES {{%tbl_ref_ket_jra}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[nilai_guna]]) REFERENCES {{%tbl_ref_nilai_guna}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[id_user_pelaksana_arsip_inaktif]]) REFERENCES {{%tbl_user_pelaksana_arsip_inaktif}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%tbl_inaktif}}');
    }
}
