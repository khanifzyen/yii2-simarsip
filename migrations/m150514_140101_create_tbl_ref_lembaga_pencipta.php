<?php

use yii\db\Schema;

class m150514_140101_create_tbl_ref_lembaga_pencipta extends \yii\db\Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%tbl_ref_lembaga_pencipta}}', [
            'id' => Schema::TYPE_PK,
            'kode_lembaga' => Schema::TYPE_STRING . '(45)',
            'nama_instansi' => Schema::TYPE_STRING . '(100) NOT NULL',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%tbl_ref_lembaga_pencipta}}');
    }
}
