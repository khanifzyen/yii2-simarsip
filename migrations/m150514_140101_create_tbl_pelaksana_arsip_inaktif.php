<?php

use yii\db\Schema;

class m150514_140101_create_tbl_pelaksana_arsip_inaktif extends \yii\db\Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%tbl_pelaksana_arsip_inaktif}}', [
            'id' => Schema::TYPE_PK,
            'id_ref_lembaga_pencipta' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'tahun_olah' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'tahun_akuisisi' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'pengolahan_ke' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'seri_dpa' => Schema::TYPE_STRING . '(45)',
            'hasil_olahan' => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 1',
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'FOREIGN KEY ([[id_ref_lembaga_pencipta]]) REFERENCES {{%tbl_ref_lembaga_pencipta}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[hasil_olahan]]) REFERENCES {{%tbl_ref_hasil_olahan}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%tbl_pelaksana_arsip_inaktif}}');
    }
}
