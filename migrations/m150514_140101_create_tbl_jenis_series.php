<?php

use yii\db\Schema;

class m150514_140101_create_tbl_jenis_series extends \yii\db\Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%tbl_jenis_series}}', [
            'id' => Schema::TYPE_PK,
            'nama' => Schema::TYPE_STRING . '(45) NOT NULL',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%tbl_jenis_series}}');
    }
}
