<?php

use yii\db\Schema;

class m150514_080101_initial_schema extends \yii\db\Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%auth_rule}}', [
            'name' => Schema::TYPE_STRING . '(64) NOT NULL',
            'data' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_INTEGER . '(11)',
            'updated_at' => Schema::TYPE_INTEGER . '(11)',
            'PRIMARY KEY ([[name]])',
        ], $tableOptions);
        
        $this->createTable('{{%auth_item}}', [
            'name' => Schema::TYPE_STRING . '(64) NOT NULL',
            'type' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'description' => Schema::TYPE_TEXT,
            'rule_name' => Schema::TYPE_STRING . '(64)',
            'data' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_INTEGER . '(11)',
            'updated_at' => Schema::TYPE_INTEGER . '(11)',
            'PRIMARY KEY ([[name]])',
            'FOREIGN KEY ([[rule_name]]) REFERENCES {{%auth_rule}} ([[name]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);
        
        $this->createTable('{{%auth_assignment}}', [
            'item_name' => Schema::TYPE_STRING . '(64) NOT NULL',
            'user_id' => Schema::TYPE_STRING . '(64) NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . '(11)',
            'PRIMARY KEY ([[item_name]], [[user_id]])',
            'FOREIGN KEY ([[item_name]]) REFERENCES {{%auth_item}} ([[name]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);
        
        $this->createTable('{{%auth_item_child}}', [
            'parent' => Schema::TYPE_STRING . '(64) NOT NULL',
            'child' => Schema::TYPE_STRING . '(64) NOT NULL',
            'PRIMARY KEY ([[parent]], [[child]])',
            'FOREIGN KEY ([[child]]) REFERENCES {{%auth_item}} ([[name]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);
        
        $this->createTable('{{%chat}}', [
            'id' => Schema::TYPE_PK,
            'userId' => Schema::TYPE_INTEGER . '(11)',
            'message' => Schema::TYPE_TEXT,
            'updateDate' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ], $tableOptions);
        
        $this->createTable('{{%log_upload}}', [
            'id' => Schema::TYPE_PK,
            'userId' => Schema::TYPE_INTEGER . '(11)',
            'title' => Schema::TYPE_STRING . '(255)',
            'filename' => Schema::TYPE_STRING . '(255)',
            'fileori' => Schema::TYPE_STRING . '(255)',
            'params' => Schema::TYPE_TEXT,
            'values' => Schema::TYPE_TEXT,
            'warning' => Schema::TYPE_TEXT,
            'keys' => Schema::TYPE_TEXT,
            'type' => Schema::TYPE_SMALLINT . '(6)',
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
            'updateDate' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'createDate' => Schema::TYPE_DATETIME,
        ], $tableOptions);
        
        $this->createTable('{{%migration}}', [
            'version' => Schema::TYPE_STRING . '(180) NOT NULL',
            'apply_time' => Schema::TYPE_INTEGER . '(11)',
            'PRIMARY KEY ([[version]])',
        ], $tableOptions);
        
        $this->createTable('{{%notification}}', [
            'id' => Schema::TYPE_PK,
            'userId' => Schema::TYPE_INTEGER . '(11)',
            'title' => Schema::TYPE_STRING . '(255)',
            'message' => Schema::TYPE_TEXT,
            'url' => Schema::TYPE_STRING . '(255)',
            'params' => Schema::TYPE_TEXT,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
            'updateDate' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'createDate' => Schema::TYPE_DATETIME,
        ], $tableOptions);
        
        $this->createTable('{{%setting}}', [
            'id' => Schema::TYPE_PK,
            'emailAdmin' => Schema::TYPE_STRING . '(255)',
            'emailSupport' => Schema::TYPE_STRING . '(255)',
            'emailOrder' => Schema::TYPE_STRING . '(255)',
            'sendgridUsername' => Schema::TYPE_STRING . '(255)',
            'sendgridPassword' => Schema::TYPE_STRING . '(255)',
            'whatsappNumber' => Schema::TYPE_STRING . '(255)',
            'whatsappPassword' => Schema::TYPE_STRING . '(255)',
            'whatsappSend' => Schema::TYPE_STRING . '(255)',
            'facebook' => Schema::TYPE_STRING . '(255)',
            'instagram' => Schema::TYPE_STRING . '(255)',
            'google' => Schema::TYPE_STRING . '(255)',
            'twitter' => Schema::TYPE_STRING . '(255)',
            'privacyPolicy' => Schema::TYPE_TEXT,
            'terms' => Schema::TYPE_TEXT,
            'legalNotice' => Schema::TYPE_TEXT,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_ref_kelompok_instansi}}', [
            'id' => Schema::TYPE_PK,
            'nama' => Schema::TYPE_STRING . '(60) NOT NULL',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_ref_kota}}', [
            'id' => Schema::TYPE_PK,
            'nama' => Schema::TYPE_STRING . '(60) NOT NULL',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_ref_negara}}', [
            'id' => Schema::TYPE_PK,
            'nama' => Schema::TYPE_STRING . '(60) NOT NULL',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_daftar_instansi}}', [
            'id' => Schema::TYPE_PK,
            'nama_instansi' => Schema::TYPE_STRING . '(255) NOT NULL',
            'alamat' => Schema::TYPE_STRING . '(255)',
            'nama_kota' => Schema::TYPE_INTEGER . '(11)',
            'kelompok' => Schema::TYPE_INTEGER . '(11)',
            'negara' => Schema::TYPE_INTEGER . '(11)',
            'telepon' => Schema::TYPE_STRING . '(45)',
            'kode_pos' => Schema::TYPE_STRING . '(10)',
            'fax' => Schema::TYPE_STRING . '(45)',
            'email' => Schema::TYPE_STRING . '(100)',
            'website' => Schema::TYPE_STRING . '(100)',
            'FOREIGN KEY ([[kelompok]]) REFERENCES {{%tbl_ref_kelompok_instansi}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[nama_kota]]) REFERENCES {{%tbl_ref_kota}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[negara]]) REFERENCES {{%tbl_ref_negara}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_jenis_arsip}}', [
            'id' => Schema::TYPE_PK,
            'nama' => Schema::TYPE_STRING . '(45) NOT NULL',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_ref_ket_jra}}', [
            'id' => Schema::TYPE_PK,
            'nama' => Schema::TYPE_STRING . '(50) NOT NULL',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_ref_masalah1}}', [
            'id' => Schema::TYPE_PK,
            'nama' => Schema::TYPE_STRING . '(255) NOT NULL',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_ref_masalah2}}', [
            'id' => Schema::TYPE_PK,
            'nama' => Schema::TYPE_STRING . '(255) NOT NULL',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_ref_nilai_guna}}', [
            'id' => Schema::TYPE_PK,
            'nama' => Schema::TYPE_STRING . '(45) NOT NULL',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_klasifikasi}}', [
            'id' => Schema::TYPE_PK,
            'klasifikasi' => Schema::TYPE_STRING . '(10)',
            'klasifikasi1' => Schema::TYPE_STRING . '(10)',
            'klasifikasi2' => Schema::TYPE_STRING . '(10)',
            'masalah1' => Schema::TYPE_INTEGER . '(11)',
            'masalah2' => Schema::TYPE_INTEGER . '(11)',
            'sub_masalah' => Schema::TYPE_STRING . '(255)',
            'indeks' => Schema::TYPE_STRING . '(255)',
            'series' => Schema::TYPE_INTEGER . '(11)',
            'raktif' => Schema::TYPE_INTEGER . '(11)',
            'rinaktif' => Schema::TYPE_INTEGER . '(11)',
            'ket_jra' => Schema::TYPE_INTEGER . '(11)',
            'nilai_guna' => Schema::TYPE_INTEGER . '(11)',
            'FOREIGN KEY ([[ket_jra]]) REFERENCES {{%tbl_ref_ket_jra}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[masalah1]]) REFERENCES {{%tbl_ref_masalah1}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[series]]) REFERENCES {{%tbl_ref_masalah2}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[nilai_guna]]) REFERENCES {{%tbl_ref_nilai_guna}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_nama_berkas}}', [
            'id' => Schema::TYPE_PK,
            'nama' => Schema::TYPE_STRING . '(60) NOT NULL',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_ref_sifat_surat}}', [
            'id' => Schema::TYPE_PK,
            'nama' => Schema::TYPE_STRING . '(60) NOT NULL',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_STRING . '(45)',
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_ref_tindakan}}', [
            'id' => Schema::TYPE_PK,
            'nama' => Schema::TYPE_STRING . '(100) NOT NULL',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_ref_tk_perkembangan}}', [
            'id' => Schema::TYPE_PK,
            'nama' => Schema::TYPE_STRING . '(100) NOT NULL',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_tempat_penyimpanan}}', [
            'id' => Schema::TYPE_PK,
            'nama' => Schema::TYPE_STRING . '(45) NOT NULL',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_unit_kerja}}', [
            'id' => Schema::TYPE_PK,
            'nama_unit_kerja' => Schema::TYPE_STRING . '(100) NOT NULL',
            'nama_pimpinan' => Schema::TYPE_STRING . '(100)',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_arsip}}', [
            'id' => Schema::TYPE_PK,
            'no_urut' => Schema::TYPE_INTEGER . '(11)',
            'id_dari_kepada' => Schema::TYPE_INTEGER . '(11)',
            'klasifikasi' => Schema::TYPE_INTEGER . '(11)',
            'no_surat' => Schema::TYPE_STRING . '(255)',
            'id_nama_berkas' => Schema::TYPE_INTEGER . '(11)',
            'tgl_surat' => Schema::TYPE_DATE,
            'tgl_terima' => Schema::TYPE_DATE,
            'isi' => Schema::TYPE_STRING . '(255)',
            'raktif' => Schema::TYPE_INTEGER . '(11)',
            'rinaktif' => Schema::TYPE_INTEGER . '(11)',
            'tahun_raktif' => Schema::TYPE_INTEGER . '(11)',
            'tahun_rinaktif' => Schema::TYPE_INTEGER . '(11)',
            'id_ket_jra' => Schema::TYPE_INTEGER . '(11)',
            'id_nilai_guna' => Schema::TYPE_INTEGER . '(11)',
            'id_tempat_penyimpanan' => Schema::TYPE_INTEGER . '(11)',
            'id_tk_perkembangan' => Schema::TYPE_INTEGER . '(11)',
            'tgl_terus' => Schema::TYPE_DATE,
            'id_unit_kerja' => Schema::TYPE_INTEGER . '(11)',
            'perihal' => Schema::TYPE_STRING . '(255)',
            'catatan_disposisi' => Schema::TYPE_STRING . '(255)',
            'id_sifat_surat' => Schema::TYPE_INTEGER . '(11)',
            'id_tindakan' => Schema::TYPE_INTEGER . '(11)',
            'id_jenis_arsip' => Schema::TYPE_INTEGER . '(11)',
            'scan_arsip' => Schema::TYPE_STRING . '(255)',
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_DATETIME,
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
            'FOREIGN KEY ([[id_dari_kepada]]) REFERENCES {{%tbl_daftar_instansi}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[id_jenis_arsip]]) REFERENCES {{%tbl_jenis_arsip}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[klasifikasi]]) REFERENCES {{%tbl_klasifikasi}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[id_nama_berkas]]) REFERENCES {{%tbl_nama_berkas}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[id_ket_jra]]) REFERENCES {{%tbl_ref_ket_jra}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[id_nilai_guna]]) REFERENCES {{%tbl_ref_nilai_guna}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[id_sifat_surat]]) REFERENCES {{%tbl_ref_sifat_surat}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[id_tindakan]]) REFERENCES {{%tbl_ref_tindakan}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[id_tk_perkembangan]]) REFERENCES {{%tbl_ref_tk_perkembangan}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[id_tempat_penyimpanan]]) REFERENCES {{%tbl_tempat_penyimpanan}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[id_unit_kerja]]) REFERENCES {{%tbl_unit_kerja}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_dynagrid}}', [
            'id' => Schema::TYPE_STRING . '(255) NOT NULL',
            'filter_id' => Schema::TYPE_STRING . '(255)',
            'sort_id' => Schema::TYPE_STRING . '(255)',
            'data' => Schema::TYPE_TEXT . ' NOT NULL',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_dynagrid_dtl}}', [
            'id' => Schema::TYPE_STRING . '(255) NOT NULL',
            'category' => Schema::TYPE_STRING . '(255) NOT NULL',
            'name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'data' => Schema::TYPE_TEXT . ' NOT NULL',
            'dynagrid_id' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);
        
        $this->createTable('{{%user}}', [
            'id' => Schema::TYPE_PK,
            'username' => Schema::TYPE_STRING . '(255) NOT NULL',
            'auth_key' => Schema::TYPE_STRING . '(255)',
            'password_hash' => Schema::TYPE_STRING . '(255)',
            'password_reset_token' => Schema::TYPE_STRING . '(255)',
            'email' => Schema::TYPE_STRING . '(255)',
            'name' => Schema::TYPE_STRING . '(255)',
            'avatar' => Schema::TYPE_STRING . '(255)',
            'phone' => Schema::TYPE_STRING . '(255)',
            'city' => Schema::TYPE_STRING . '(255)',
            'role' => Schema::TYPE_SMALLINT . '(6)',
            'status' => Schema::TYPE_SMALLINT . '(6)',
            'banner_top' => Schema::TYPE_STRING . '(255)',
            'position' => Schema::TYPE_STRING . '(255)',
            'hobby' => Schema::TYPE_SMALLINT . '(6)',
            'params' => Schema::TYPE_TEXT,
            'description' => Schema::TYPE_TEXT,
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ], $tableOptions);
        
        $this->createTable('{{%tbl_product}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(45) NOT NULL',
            'id_user' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'createDate' => Schema::TYPE_TIMESTAMP,
            'updateDate' => Schema::TYPE_TIMESTAMP,
            'userUpdate' => Schema::TYPE_INTEGER . '(11)',
            'userCreate' => Schema::TYPE_INTEGER . '(11)',
            'FOREIGN KEY ([[id_user]]) REFERENCES {{%user}} ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);
        
        $this->createTable('{{%todolist}}', [
            'id' => Schema::TYPE_PK,
            'userId' => Schema::TYPE_INTEGER . '(11)',
            'title' => Schema::TYPE_STRING . '(255)',
            'status' => Schema::TYPE_SMALLINT . '(6)',
            'params' => Schema::TYPE_TEXT,
            'createDate' => Schema::TYPE_DATETIME,
            'updateDate' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%todolist}}');
        $this->dropTable('{{%tbl_product}}');
        $this->dropTable('{{%user}}');
        $this->dropTable('{{%tbl_dynagrid_dtl}}');
        $this->dropTable('{{%tbl_dynagrid}}');
        $this->dropTable('{{%tbl_arsip}}');
        $this->dropTable('{{%tbl_unit_kerja}}');
        $this->dropTable('{{%tbl_tempat_penyimpanan}}');
        $this->dropTable('{{%tbl_ref_tk_perkembangan}}');
        $this->dropTable('{{%tbl_ref_tindakan}}');
        $this->dropTable('{{%tbl_ref_sifat_surat}}');
        $this->dropTable('{{%tbl_nama_berkas}}');
        $this->dropTable('{{%tbl_klasifikasi}}');
        $this->dropTable('{{%tbl_ref_nilai_guna}}');
        $this->dropTable('{{%tbl_ref_masalah2}}');
        $this->dropTable('{{%tbl_ref_masalah1}}');
        $this->dropTable('{{%tbl_ref_ket_jra}}');
        $this->dropTable('{{%tbl_jenis_arsip}}');
        $this->dropTable('{{%tbl_daftar_instansi}}');
        $this->dropTable('{{%tbl_ref_negara}}');
        $this->dropTable('{{%tbl_ref_kota}}');
        $this->dropTable('{{%tbl_ref_kelompok_instansi}}');
        $this->dropTable('{{%setting}}');
        $this->dropTable('{{%notification}}');
        $this->dropTable('{{%migration}}');
        $this->dropTable('{{%log_upload}}');
        $this->dropTable('{{%chat}}');
        $this->dropTable('{{%auth_item_child}}');
        $this->dropTable('{{%auth_assignment}}');
        $this->dropTable('{{%auth_item}}');
        $this->dropTable('{{%auth_rule}}');
    }
}
