<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PelaksanaArsipInaktif;

/**
 * PelaksanaArsipInaktifSearch represents the model behind the search form about `app\models\PelaksanaArsipInaktif`.
 */
class PelaksanaArsipInaktifSearch extends PelaksanaArsipInaktif
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_ref_lembaga_pencipta', 'tahun_olah', 'tahun_akuisisi', 'pengolahan_ke', 'hasil_olahan', 'userCreate', 'userUpdate'], 'integer'],
            [['seri_dpa', 'createDate', 'updateDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PelaksanaArsipInaktif::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_ref_lembaga_pencipta' => $this->id_ref_lembaga_pencipta,
            'tahun_olah' => $this->tahun_olah,
            'tahun_akuisisi' => $this->tahun_akuisisi,
            'pengolahan_ke' => $this->pengolahan_ke,
            'hasil_olahan' => $this->hasil_olahan,
            'userCreate' => $this->userCreate,
            'userUpdate' => $this->userUpdate,
            'createDate' => $this->createDate,
            'updateDate' => $this->updateDate,
        ]);

        $query->andFilterWhere(['like', 'seri_dpa', $this->seri_dpa]);

        return $dataProvider;
    }
}
