<?php

namespace app\models;

use Yii;
use app\models\User;


/**
 * This is the model class for table "{{%tbl_daftar_instansi}}".
 *
 * @property integer $id
 * @property string $nama_instansi
 * @property string $alamat
 * @property integer $nama_kota
 * @property string $kelompok
 * @property string $negara
 * @property string $telepon
 * @property string $kode_pos
 * @property string $fax
 * @property string $email
 * @property string $website
 *
 * @property TblRefKota $namaKota
 */
class DaftarInstansi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_daftar_instansi}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_instansi'], 'required'],
            [['nama_kota'], 'integer'],
            [['nama_instansi', 'alamat'], 'string', 'max' => 255],
            [['kelompok', 'negara', 'telepon', 'fax'], 'string', 'max' => 45],
            [['kode_pos'], 'string', 'max' => 10],
            [['email', 'website'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama_instansi' => Yii::t('app', 'Nama Instansi'),
            'alamat' => Yii::t('app', 'Alamat'),
            'nama_kota' => Yii::t('app', 'Nama Kota'),
            'kelompok' => Yii::t('app', 'Kelompok'),
            'negara' => Yii::t('app', 'Negara'),
            'telepon' => Yii::t('app', 'Telepon'),
            'kode_pos' => Yii::t('app', 'Kode Pos'),
            'fax' => Yii::t('app', 'Fax'),
            'email' => Yii::t('app', 'Email'),
            'website' => Yii::t('app', 'Website'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNamaKota()
    {
        return $this->hasOne(RefKota::className(), ['id' => 'nama_kota']);
    }
    
    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->createDate = date('Y-m-d H:i:s');
            $this->userCreate = Yii::$app->user->id;
            $this->userUpdate = Yii::$app->user->id;
        } else {
            $this->updateDate = date('Y-m-d H:i:s');
            $this->userUpdate = Yii::$app->user->id;
        }
        return parent::beforeSave($insert);
    }
    
    public function getUserCreateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userCreate])->one();
        return $user->username;
    }

    public function getUserUpdateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userUpdate])->one();
        return $user->username;
    }
    
    }
