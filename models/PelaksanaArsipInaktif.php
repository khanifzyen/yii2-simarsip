<?php

namespace app\models;

use Yii;
use app\models\User;


/**
 * This is the model class for table "{{%tbl_pelaksana_arsip_inaktif}}".
 *
 * @property integer $id
 * @property integer $id_ref_lembaga_pencipta
 * @property integer $tahun_olah
 * @property integer $tahun_akuisisi
 * @property integer $pengolahan_ke
 * @property string $seri_dpa
 * @property integer $hasil_olahan
 * @property integer $userCreate
 * @property integer $userUpdate
 * @property string $createDate
 * @property string $updateDate
 *
 * @property TblInaktif[] $tblInaktifs
 * @property TblRefLembagaPencipta $idRefLembagaPencipta
 * @property TblRefHasilOlahan $hasilOlahan
 * @property TblUserPelaksanaArsipInaktif[] $tblUserPelaksanaArsipInaktifs
 */
class PelaksanaArsipInaktif extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_pelaksana_arsip_inaktif}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_ref_lembaga_pencipta', 'tahun_olah', 'tahun_akuisisi', 'pengolahan_ke'], 'required'],
            [['id_ref_lembaga_pencipta', 'tahun_olah', 'tahun_akuisisi', 'pengolahan_ke', 'hasil_olahan', 'userCreate', 'userUpdate'], 'integer'],
            [['createDate', 'updateDate'], 'safe'],
            [['seri_dpa'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_ref_lembaga_pencipta' => Yii::t('app', 'Id Ref Lembaga Pencipta'),
            'tahun_olah' => Yii::t('app', 'Tahun Olah'),
            'tahun_akuisisi' => Yii::t('app', 'Tahun Akuisisi'),
            'pengolahan_ke' => Yii::t('app', 'Pengolahan Ke'),
            'seri_dpa' => Yii::t('app', 'Seri Dpa'),
            'hasil_olahan' => Yii::t('app', 'Hasil Olahan'),
            'userCreate' => Yii::t('app', 'User Create'),
            'userUpdate' => Yii::t('app', 'User Update'),
            'createDate' => Yii::t('app', 'Create Date'),
            'updateDate' => Yii::t('app', 'Update Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblInaktifs()
    {
        return $this->hasMany(TblInaktif::className(), ['id_pelaksana_arsip_inaktif' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRefLembagaPencipta()
    {
        return $this->hasOne(TblRefLembagaPencipta::className(), ['id' => 'id_ref_lembaga_pencipta']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHasilOlahan()
    {
        return $this->hasOne(TblRefHasilOlahan::className(), ['id' => 'hasil_olahan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblUserPelaksanaArsipInaktifs()
    {
        return $this->hasMany(TblUserPelaksanaArsipInaktif::className(), ['id_pelaksana_arsip_inaktif' => 'id']);
    }
    
    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->createDate = date('Y-m-d H:i:s');
            $this->userCreate = Yii::$app->user->id;
            $this->userUpdate = Yii::$app->user->id;
        } else {
            $this->updateDate = date('Y-m-d H:i:s');
            $this->userUpdate = Yii::$app->user->id;
        }
        return parent::beforeSave($insert);
    }
    
    public function getUserCreateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userCreate])->one();
        return $user->username;
    }

    public function getUserUpdateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userUpdate])->one();
        return $user->username;
    }
    
    }
