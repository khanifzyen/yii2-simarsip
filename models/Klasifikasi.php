<?php

namespace app\models;

use Yii;
use app\models\User;


/**
 * This is the model class for table "{{%tbl_klasifikasi}}".
 *
 * @property integer $id
 * @property string $klasifikasi
 * @property string $klasifikasi1
 * @property string $klasifikasi2
 * @property integer $masalah1
 * @property integer $masalah2
 * @property string $sub_masalah
 * @property string $indeks
 * @property integer $series
 * @property integer $raktif
 * @property integer $rinaktif
 * @property integer $ket_jra
 * @property integer $nilai_guna
 *
 * @property TblArsip[] $tblArsips
 * @property TblRefKetJra $ketJra
 * @property TblRefMasalah1 $masalah10
 * @property TblRefMasalah2 $masalah20
 * @property TblRefMasalah2 $series0
 * @property TblRefNilaiGuna $nilaiGuna
 */
class Klasifikasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_klasifikasi}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['masalah1', 'masalah2', 'series', 'raktif', 'rinaktif', 'ket_jra', 'nilai_guna'], 'integer'],
            [['klasifikasi', 'klasifikasi1', 'klasifikasi2'], 'string', 'max' => 10],
            [['sub_masalah', 'indeks'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'klasifikasi' => Yii::t('app', 'Klasifikasi'),
            'klasifikasi1' => Yii::t('app', 'Klasifikasi1'),
            'klasifikasi2' => Yii::t('app', 'Klasifikasi2'),
            'masalah1' => Yii::t('app', 'Masalah1'),
            'masalah2' => Yii::t('app', 'Masalah2'),
            'sub_masalah' => Yii::t('app', 'Sub Masalah'),
            'indeks' => Yii::t('app', 'Indeks'),
            'series' => Yii::t('app', 'Series'),
            'raktif' => Yii::t('app', 'Raktif'),
            'rinaktif' => Yii::t('app', 'Rinaktif'),
            'ket_jra' => Yii::t('app', 'Ket Jra'),
            'nilai_guna' => Yii::t('app', 'Nilai Guna'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblArsips()
    {
        return $this->hasMany(TblArsip::className(), ['klasifikasi' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKetJra()
    {
        return $this->hasOne(TblRefKetJra::className(), ['id' => 'ket_jra']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasalah10()
    {
        return $this->hasOne(TblRefMasalah1::className(), ['id' => 'masalah1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasalah20()
    {
        return $this->hasOne(TblRefMasalah2::className(), ['id' => 'masalah2']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeries0()
    {
        return $this->hasOne(TblRefMasalah2::className(), ['id' => 'series']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNilaiGuna()
    {
        return $this->hasOne(TblRefNilaiGuna::className(), ['id' => 'nilai_guna']);
    }
    
    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->createDate = date('Y-m-d H:i:s');
            $this->userCreate = Yii::$app->user->id;
            $this->userUpdate = Yii::$app->user->id;
        } else {
            $this->updateDate = date('Y-m-d H:i:s');
            $this->userUpdate = Yii::$app->user->id;
        }
        return parent::beforeSave($insert);
    }
    
    public function getUserCreateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userCreate])->one();
        return $user->username;
    }

    public function getUserUpdateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userUpdate])->one();
        return $user->username;
    }
    
    }
