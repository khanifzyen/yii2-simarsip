<?php

namespace app\models;

use Yii;
use app\models\User;


/**
 * This is the model class for table "{{%tbl_inaktif}}".
 *
 * @property integer $id
 * @property integer $id_pelaksana_arsip_inaktif
 * @property string $kode_pelaksana
 * @property integer $no_fisis
 * @property integer $id_klasifikasi
 * @property string $isi_arsip
 * @property integer $bulan1
 * @property integer $tahun1
 * @property integer $bulan2
 * @property integer $tahun2
 * @property integer $perlengkapan
 * @property integer $tk_perkembangan
 * @property integer $media_id
 * @property integer $id_jenis_series
 * @property integer $tahun_aktif
 * @property string $tahun_inaktif
 * @property string $tahun_retensi
 * @property integer $ket_jra
 * @property integer $nilai_guna
 * @property string $createDate
 * @property string $updateDate
 * @property integer $userCreate
 * @property integer $userUpdate
 * @property integer $id_user_pelaksana_arsip_inaktif
 *
 * @property TblPelaksanaArsipInaktif $idPelaksanaArsipInaktif
 * @property TblKlasifikasi $idKlasifikasi
 * @property TblRefPerlengkapan $perlengkapan0
 * @property TblRefTkPerkembangan $tkPerkembangan
 * @property TblRefMedia $media
 * @property TblJenisSeries $idJenisSeries
 * @property TblRefKetJra $ketJra
 * @property TblRefNilaiGuna $nilaiGuna
 * @property TblUserPelaksanaArsipInaktif $idUserPelaksanaArsipInaktif
 */
class Inaktif extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_inaktif}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pelaksana_arsip_inaktif', 'id_klasifikasi', 'isi_arsip', 'perlengkapan', 'tk_perkembangan', 'media_id', 'id_jenis_series'], 'required'],
            [['id_pelaksana_arsip_inaktif', 'no_fisis', 'id_klasifikasi', 'bulan1', 'tahun1', 'bulan2', 'tahun2', 'perlengkapan', 'tk_perkembangan', 'media_id', 'id_jenis_series', 'tahun_aktif', 'ket_jra', 'nilai_guna', 'userCreate', 'userUpdate', 'id_user_pelaksana_arsip_inaktif'], 'integer'],
            [['isi_arsip'], 'string'],
            [['createDate', 'updateDate'], 'safe'],
            [['kode_pelaksana', 'tahun_inaktif', 'tahun_retensi'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_pelaksana_arsip_inaktif' => Yii::t('app', 'Id Pelaksana Arsip Inaktif'),
            'kode_pelaksana' => Yii::t('app', 'Kode Pelaksana'),
            'no_fisis' => Yii::t('app', 'No Fisis'),
            'id_klasifikasi' => Yii::t('app', 'Id Klasifikasi'),
            'isi_arsip' => Yii::t('app', 'Isi Arsip'),
            'bulan1' => Yii::t('app', 'Bulan1'),
            'tahun1' => Yii::t('app', 'Tahun1'),
            'bulan2' => Yii::t('app', 'Bulan2'),
            'tahun2' => Yii::t('app', 'Tahun2'),
            'perlengkapan' => Yii::t('app', 'Perlengkapan'),
            'tk_perkembangan' => Yii::t('app', 'Tk Perkembangan'),
            'media_id' => Yii::t('app', 'Media ID'),
            'id_jenis_series' => Yii::t('app', 'Id Jenis Series'),
            'tahun_aktif' => Yii::t('app', 'Tahun Aktif'),
            'tahun_inaktif' => Yii::t('app', 'Tahun Inaktif'),
            'tahun_retensi' => Yii::t('app', 'Tahun Retensi'),
            'ket_jra' => Yii::t('app', 'Ket Jra'),
            'nilai_guna' => Yii::t('app', 'Nilai Guna'),
            'createDate' => Yii::t('app', 'Create Date'),
            'updateDate' => Yii::t('app', 'Update Date'),
            'userCreate' => Yii::t('app', 'User Create'),
            'userUpdate' => Yii::t('app', 'User Update'),
            'id_user_pelaksana_arsip_inaktif' => Yii::t('app', 'Id User Pelaksana Arsip Inaktif'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPelaksanaArsipInaktif()
    {
        return $this->hasOne(TblPelaksanaArsipInaktif::className(), ['id' => 'id_pelaksana_arsip_inaktif']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdKlasifikasi()
    {
        return $this->hasOne(TblKlasifikasi::className(), ['id' => 'id_klasifikasi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerlengkapan0()
    {
        return $this->hasOne(TblRefPerlengkapan::className(), ['id' => 'perlengkapan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTkPerkembangan()
    {
        return $this->hasOne(TblRefTkPerkembangan::className(), ['id' => 'tk_perkembangan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedia()
    {
        return $this->hasOne(TblRefMedia::className(), ['int' => 'media_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdJenisSeries()
    {
        return $this->hasOne(TblJenisSeries::className(), ['id' => 'id_jenis_series']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKetJra()
    {
        return $this->hasOne(TblRefKetJra::className(), ['id' => 'ket_jra']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNilaiGuna()
    {
        return $this->hasOne(TblRefNilaiGuna::className(), ['id' => 'nilai_guna']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserPelaksanaArsipInaktif()
    {
        return $this->hasOne(TblUserPelaksanaArsipInaktif::className(), ['id' => 'id_user_pelaksana_arsip_inaktif']);
    }
    
    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->createDate = date('Y-m-d H:i:s');
            $this->userCreate = Yii::$app->user->id;
            $this->userUpdate = Yii::$app->user->id;
        } else {
            $this->updateDate = date('Y-m-d H:i:s');
            $this->userUpdate = Yii::$app->user->id;
        }
        return parent::beforeSave($insert);
    }
    
    public function getUserCreateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userCreate])->one();
        return $user->username;
    }

    public function getUserUpdateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userUpdate])->one();
        return $user->username;
    }
    
    }
