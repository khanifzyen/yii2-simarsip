<?php

namespace app\models;

use Yii;
use app\models\User;


/**
 * This is the model class for table "{{%tbl_jenis_series}}".
 *
 * @property integer $id
 * @property string $nama
 * @property string $createDate
 * @property string $updateDate
 * @property integer $userUpdate
 * @property integer $userCreate
 *
 * @property TblInaktif[] $tblInaktifs
 */
class JenisSeries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_jenis_series}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['createDate', 'updateDate'], 'safe'],
            [['userUpdate', 'userCreate'], 'integer'],
            [['nama'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'createDate' => Yii::t('app', 'Create Date'),
            'updateDate' => Yii::t('app', 'Update Date'),
            'userUpdate' => Yii::t('app', 'User Update'),
            'userCreate' => Yii::t('app', 'User Create'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblInaktifs()
    {
        return $this->hasMany(TblInaktif::className(), ['id_jenis_series' => 'id']);
    }
    
    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->createDate = date('Y-m-d H:i:s');
            $this->userCreate = Yii::$app->user->id;
            $this->userUpdate = Yii::$app->user->id;
        } else {
            $this->updateDate = date('Y-m-d H:i:s');
            $this->userUpdate = Yii::$app->user->id;
        }
        return parent::beforeSave($insert);
    }
    
    public function getUserCreateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userCreate])->one();
        return $user->username;
    }

    public function getUserUpdateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userUpdate])->one();
        return $user->username;
    }
    
    }
