<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RefLembagaPencipta;

/**
 * RefLembagaPenciptaSearch represents the model behind the search form about `app\models\RefLembagaPencipta`.
 */
class RefLembagaPenciptaSearch extends RefLembagaPencipta
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'userCreate', 'userUpdate'], 'integer'],
            [['kode_lembaga', 'nama_instansi', 'createDate', 'updateDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RefLembagaPencipta::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'createDate' => $this->createDate,
            'updateDate' => $this->updateDate,
            'userCreate' => $this->userCreate,
            'userUpdate' => $this->userUpdate,
        ]);

        $query->andFilterWhere(['like', 'kode_lembaga', $this->kode_lembaga])
            ->andFilterWhere(['like', 'nama_instansi', $this->nama_instansi]);

        return $dataProvider;
    }
}
