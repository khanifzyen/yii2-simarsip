<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserPelaksanaArsipInaktif;

/**
 * UserPelaksanaArsipInaktifSearch represents the model behind the search form about `app\models\UserPelaksanaArsipInaktif`.
 */
class UserPelaksanaArsipInaktifSearch extends UserPelaksanaArsipInaktif
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_pelaksana_arsip_inaktif', 'userCreate', 'userUpdate'], 'integer'],
            [['createDate', 'updateDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserPelaksanaArsipInaktif::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_user' => $this->id_user,
            'id_pelaksana_arsip_inaktif' => $this->id_pelaksana_arsip_inaktif,
            'createDate' => $this->createDate,
            'updateDate' => $this->updateDate,
            'userCreate' => $this->userCreate,
            'userUpdate' => $this->userUpdate,
        ]);

        return $dataProvider;
    }
}
