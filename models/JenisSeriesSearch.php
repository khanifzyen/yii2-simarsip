<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\JenisSeries;

/**
 * JenisSeriesSearch represents the model behind the search form about `app\models\JenisSeries`.
 */
class JenisSeriesSearch extends JenisSeries
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'userUpdate', 'userCreate'], 'integer'],
            [['nama', 'createDate', 'updateDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JenisSeries::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'createDate' => $this->createDate,
            'updateDate' => $this->updateDate,
            'userUpdate' => $this->userUpdate,
            'userCreate' => $this->userCreate,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama]);

        return $dataProvider;
    }
}
