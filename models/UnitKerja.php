<?php

namespace app\models;

use Yii;
use app\models\User;


/**
 * This is the model class for table "{{%tbl_unit_kerja}}".
 *
 * @property integer $id
 * @property string $nama_unit_kerja
 * @property string $nama_pimpinan
 * @property string $createDate
 * @property string $updateDate
 * @property integer $userCreate
 * @property integer $userUpdate
 *
 * @property TblArsip[] $tblArsips
 */
class UnitKerja extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_unit_kerja}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_unit_kerja'], 'required'],
            [['createDate', 'updateDate'], 'safe'],
            [['userCreate', 'userUpdate'], 'integer'],
            [['nama_unit_kerja', 'nama_pimpinan'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama_unit_kerja' => Yii::t('app', 'Nama Unit Kerja'),
            'nama_pimpinan' => Yii::t('app', 'Nama Pimpinan'),
            'createDate' => Yii::t('app', 'Create Date'),
            'updateDate' => Yii::t('app', 'Update Date'),
            'userCreate' => Yii::t('app', 'User Create'),
            'userUpdate' => Yii::t('app', 'User Update'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblArsips()
    {
        return $this->hasMany(TblArsip::className(), ['id_unit_kerja' => 'id']);
    }
    
    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->createDate = date('Y-m-d H:i:s');
            $this->userCreate = Yii::$app->user->id;
            $this->userUpdate = Yii::$app->user->id;
        } else {
            $this->updateDate = date('Y-m-d H:i:s');
            $this->userUpdate = Yii::$app->user->id;
        }
        return parent::beforeSave($insert);
    }
    
    public function getUserCreateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userCreate])->one();
        return $user->username;
    }

    public function getUserUpdateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userUpdate])->one();
        return $user->username;
    }
    
    }
