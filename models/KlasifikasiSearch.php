<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Klasifikasi;

/**
 * KlasifikasiSearch represents the model behind the search form about `app\models\Klasifikasi`.
 */
class KlasifikasiSearch extends Klasifikasi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'masalah1', 'masalah2', 'series', 'raktif', 'rinaktif', 'ket_jra', 'nilai_guna'], 'integer'],
            [['klasifikasi', 'klasifikasi1', 'klasifikasi2', 'sub_masalah', 'indeks'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Klasifikasi::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'masalah1' => $this->masalah1,
            'masalah2' => $this->masalah2,
            'series' => $this->series,
            'raktif' => $this->raktif,
            'rinaktif' => $this->rinaktif,
            'ket_jra' => $this->ket_jra,
            'nilai_guna' => $this->nilai_guna,
        ]);

        $query->andFilterWhere(['like', 'klasifikasi', $this->klasifikasi])
            ->andFilterWhere(['like', 'klasifikasi1', $this->klasifikasi1])
            ->andFilterWhere(['like', 'klasifikasi2', $this->klasifikasi2])
            ->andFilterWhere(['like', 'sub_masalah', $this->sub_masalah])
            ->andFilterWhere(['like', 'indeks', $this->indeks]);

        return $dataProvider;
    }
}
