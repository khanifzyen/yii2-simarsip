<?php

namespace app\models;

use Yii;
use app\models\User;


/**
 * This is the model class for table "tbl_ref_media".
 *
 * @property integer $int
 * @property string $nama
 * @property string $createDate
 * @property string $updateDate
 * @property integer $userCreate
 * @property integer $userUpdate
 *
 * @property TblInaktif[] $tblInaktifs
 */
class RefMedia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_ref_media';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['createDate', 'updateDate'], 'safe'],
            [['userCreate', 'userUpdate'], 'integer'],
            [['nama'], 'string', 'max' => 60]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'int' => Yii::t('app', 'Int'),
            'nama' => Yii::t('app', 'Nama'),
            'createDate' => Yii::t('app', 'Create Date'),
            'updateDate' => Yii::t('app', 'Update Date'),
            'userCreate' => Yii::t('app', 'User Create'),
            'userUpdate' => Yii::t('app', 'User Update'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblInaktifs()
    {
        return $this->hasMany(TblInaktif::className(), ['media_id' => 'int']);
    }
    
    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->createDate = date('Y-m-d H:i:s');
            $this->userCreate = Yii::$app->user->id;
            $this->userUpdate = Yii::$app->user->id;
        } else {
            $this->updateDate = date('Y-m-d H:i:s');
            $this->userUpdate = Yii::$app->user->id;
        }
        return parent::beforeSave($insert);
    }
    
    public function getUserCreateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userCreate])->one();
        return $user->username;
    }

    public function getUserUpdateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userUpdate])->one();
        return $user->username;
    }
    
    }
