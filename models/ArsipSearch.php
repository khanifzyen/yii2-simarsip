<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Arsip;

/**
 * ArsipSearch represents the model behind the search form about `app\models\Arsip`.
 */
class ArsipSearch extends Arsip
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'no_urut', 'id_dari_kepada', 'klasifikasi', 'id_nama_berkas', 'raktif', 'rinaktif', 'tahun_raktif', 'tahun_rinaktif', 'id_ket_jra', 'id_nilai_guna', 'id_tempat_penyimpanan', 'id_tk_perkembangan', 'id_unit_kerja', 'id_sifat_surat', 'id_tindakan', 'id_jenis_arsip', 'userCreate', 'userUpdate'], 'integer'],
            [['no_surat', 'tgl_surat', 'tgl_terima', 'isi', 'tgl_terus', 'perihal', 'catatan_disposisi', 'scan_arsip', 'createDate', 'updateDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Arsip::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'no_urut' => $this->no_urut,
            'id_dari_kepada' => $this->id_dari_kepada,
            'klasifikasi' => $this->klasifikasi,
            'id_nama_berkas' => $this->id_nama_berkas,
            'tgl_surat' => $this->tgl_surat,
            'tgl_terima' => $this->tgl_terima,
            'raktif' => $this->raktif,
            'rinaktif' => $this->rinaktif,
            'tahun_raktif' => $this->tahun_raktif,
            'tahun_rinaktif' => $this->tahun_rinaktif,
            'id_ket_jra' => $this->id_ket_jra,
            'id_nilai_guna' => $this->id_nilai_guna,
            'id_tempat_penyimpanan' => $this->id_tempat_penyimpanan,
            'id_tk_perkembangan' => $this->id_tk_perkembangan,
            'tgl_terus' => $this->tgl_terus,
            'id_unit_kerja' => $this->id_unit_kerja,
            'id_sifat_surat' => $this->id_sifat_surat,
            'id_tindakan' => $this->id_tindakan,
            'id_jenis_arsip' => $this->id_jenis_arsip,
            'createDate' => $this->createDate,
            'updateDate' => $this->updateDate,
            'userCreate' => $this->userCreate,
            'userUpdate' => $this->userUpdate,
        ]);

        $query->andFilterWhere(['like', 'no_surat', $this->no_surat])
            ->andFilterWhere(['like', 'isi', $this->isi])
            ->andFilterWhere(['like', 'perihal', $this->perihal])
            ->andFilterWhere(['like', 'catatan_disposisi', $this->catatan_disposisi])
            ->andFilterWhere(['like', 'scan_arsip', $this->scan_arsip]);

        return $dataProvider;
    }
}
