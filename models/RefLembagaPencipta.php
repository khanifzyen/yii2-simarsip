<?php

namespace app\models;

use Yii;
use app\models\User;


/**
 * This is the model class for table "{{%tbl_ref_lembaga_pencipta}}".
 *
 * @property integer $id
 * @property string $kode_lembaga
 * @property string $nama_instansi
 * @property string $createDate
 * @property string $updateDate
 * @property integer $userCreate
 * @property integer $userUpdate
 *
 * @property TblPelaksanaArsipInaktif[] $tblPelaksanaArsipInaktifs
 */
class RefLembagaPencipta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_ref_lembaga_pencipta}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_instansi'], 'required'],
            [['createDate', 'updateDate'], 'safe'],
            [['userCreate', 'userUpdate'], 'integer'],
            [['kode_lembaga'], 'string', 'max' => 45],
            [['nama_instansi'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kode_lembaga' => Yii::t('app', 'Kode Lembaga'),
            'nama_instansi' => Yii::t('app', 'Nama Instansi'),
            'createDate' => Yii::t('app', 'Create Date'),
            'updateDate' => Yii::t('app', 'Update Date'),
            'userCreate' => Yii::t('app', 'User Create'),
            'userUpdate' => Yii::t('app', 'User Update'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblPelaksanaArsipInaktifs()
    {
        return $this->hasMany(TblPelaksanaArsipInaktif::className(), ['id_ref_lembaga_pencipta' => 'id']);
    }
    
    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->createDate = date('Y-m-d H:i:s');
            $this->userCreate = Yii::$app->user->id;
            $this->userUpdate = Yii::$app->user->id;
        } else {
            $this->updateDate = date('Y-m-d H:i:s');
            $this->userUpdate = Yii::$app->user->id;
        }
        return parent::beforeSave($insert);
    }
    
    public function getUserCreateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userCreate])->one();
        return $user->username;
    }

    public function getUserUpdateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userUpdate])->one();
        return $user->username;
    }
    
    }
