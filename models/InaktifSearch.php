<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Inaktif;

/**
 * InaktifSearch represents the model behind the search form about `app\models\Inaktif`.
 */
class InaktifSearch extends Inaktif
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pelaksana_arsip_inaktif', 'no_fisis', 'id_klasifikasi', 'bulan1', 'tahun1', 'bulan2', 'tahun2', 'perlengkapan', 'tk_perkembangan', 'media_id', 'id_jenis_series', 'tahun_aktif', 'ket_jra', 'nilai_guna', 'userCreate', 'userUpdate', 'id_user_pelaksana_arsip_inaktif'], 'integer'],
            [['kode_pelaksana', 'isi_arsip', 'tahun_inaktif', 'tahun_retensi', 'createDate', 'updateDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Inaktif::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_pelaksana_arsip_inaktif' => $this->id_pelaksana_arsip_inaktif,
            'no_fisis' => $this->no_fisis,
            'id_klasifikasi' => $this->id_klasifikasi,
            'bulan1' => $this->bulan1,
            'tahun1' => $this->tahun1,
            'bulan2' => $this->bulan2,
            'tahun2' => $this->tahun2,
            'perlengkapan' => $this->perlengkapan,
            'tk_perkembangan' => $this->tk_perkembangan,
            'media_id' => $this->media_id,
            'id_jenis_series' => $this->id_jenis_series,
            'tahun_aktif' => $this->tahun_aktif,
            'ket_jra' => $this->ket_jra,
            'nilai_guna' => $this->nilai_guna,
            'createDate' => $this->createDate,
            'updateDate' => $this->updateDate,
            'userCreate' => $this->userCreate,
            'userUpdate' => $this->userUpdate,
            'id_user_pelaksana_arsip_inaktif' => $this->id_user_pelaksana_arsip_inaktif,
        ]);

        $query->andFilterWhere(['like', 'kode_pelaksana', $this->kode_pelaksana])
            ->andFilterWhere(['like', 'isi_arsip', $this->isi_arsip])
            ->andFilterWhere(['like', 'tahun_inaktif', $this->tahun_inaktif])
            ->andFilterWhere(['like', 'tahun_retensi', $this->tahun_retensi]);

        return $dataProvider;
    }
}
