<?php

namespace app\models;

use Yii;
use app\models\User;


/**
 * This is the model class for table "{{%tbl_arsip}}".
 *
 * @property integer $id
 * @property integer $no_urut
 * @property integer $id_dari_kepada
 * @property integer $klasifikasi
 * @property string $no_surat
 * @property integer $id_nama_berkas
 * @property string $tgl_surat
 * @property string $tgl_terima
 * @property string $isi
 * @property integer $raktif
 * @property integer $rinaktif
 * @property integer $tahun_raktif
 * @property integer $tahun_rinaktif
 * @property integer $id_ket_jra
 * @property integer $id_nilai_guna
 * @property integer $id_tempat_penyimpanan
 * @property integer $id_tk_perkembangan
 * @property string $tgl_terus
 * @property integer $id_unit_kerja
 * @property string $perihal
 * @property string $catatan_disposisi
 * @property integer $id_sifat_surat
 * @property integer $id_tindakan
 * @property integer $id_jenis_arsip
 * @property string $scan_arsip
 * @property string $createDate
 * @property string $updateDate
 * @property integer $userCreate
 * @property integer $userUpdate
 *
 * @property TblDaftarInstansi $idDariKepada
 * @property TblJenisArsip $idJenisArsip
 * @property TblKlasifikasi $klasifikasi0
 * @property TblNamaBerkas $idNamaBerkas
 * @property TblRefKetJra $idKetJra
 * @property TblRefNilaiGuna $idNilaiGuna
 * @property TblRefSifatSurat $idSifatSurat
 * @property TblRefTindakan $idTindakan
 * @property TblRefTkPerkembangan $idTkPerkembangan
 * @property TblTempatPenyimpanan $idTempatPenyimpanan
 * @property TblUnitKerja $idUnitKerja
 */
class Arsip extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_arsip}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_urut', 'id_dari_kepada', 'klasifikasi', 'id_nama_berkas', 'raktif', 'rinaktif', 'tahun_raktif', 'tahun_rinaktif', 'id_ket_jra', 'id_nilai_guna', 'id_tempat_penyimpanan', 'id_tk_perkembangan', 'id_unit_kerja', 'id_sifat_surat', 'id_tindakan', 'id_jenis_arsip', 'userCreate', 'userUpdate'], 'integer'],
            [['tgl_surat', 'tgl_terima', 'tgl_terus', 'createDate', 'updateDate'], 'safe'],
            [['no_surat', 'isi', 'perihal', 'catatan_disposisi', 'scan_arsip'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'no_urut' => Yii::t('app', 'No Urut'),
            'id_dari_kepada' => Yii::t('app', 'Dari/Kepada'),
            'klasifikasi' => Yii::t('app', 'Klasifikasi'),
            'no_surat' => Yii::t('app', 'No Surat'),
            'id_nama_berkas' => Yii::t('app', 'Nama Berkas'),
            'tgl_surat' => Yii::t('app', 'Tgl Surat'),
            'tgl_terima' => Yii::t('app', 'Tgl Terima'),
            'isi' => Yii::t('app', 'Isi'),
            'raktif' => Yii::t('app', 'Raktif'),
            'rinaktif' => Yii::t('app', 'Rinaktif'),
            'tahun_raktif' => Yii::t('app', 'Tahun Raktif'),
            'tahun_rinaktif' => Yii::t('app', 'Tahun Rinaktif'),
            'id_ket_jra' => Yii::t('app', 'Ket Jra'),
            'id_nilai_guna' => Yii::t('app', 'Nilai Guna'),
            'id_tempat_penyimpanan' => Yii::t('app', 'Tempat Penyimpanan'),
            'id_tk_perkembangan' => Yii::t('app', 'Tk Perkembangan'),
            'tgl_terus' => Yii::t('app', 'Tgl Terus'),
            'id_unit_kerja' => Yii::t('app', 'Unit Kerja'),
            'perihal' => Yii::t('app', 'Perihal'),
            'catatan_disposisi' => Yii::t('app', 'Catatan Disposisi'),
            'id_sifat_surat' => Yii::t('app', 'Sifat Surat'),
            'id_tindakan' => Yii::t('app', 'Tindakan'),
            'id_jenis_arsip' => Yii::t('app', 'Jenis Arsip'),
            'scan_arsip' => Yii::t('app', 'Scan Arsip'),
            'createDate' => Yii::t('app', 'Create Date'),
            'updateDate' => Yii::t('app', 'Update Date'),
            'userCreate' => Yii::t('app', 'User Create'),
            'userUpdate' => Yii::t('app', 'User Update'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDariKepada()
    {
        return $this->hasOne(TblDaftarInstansi::className(), ['id' => 'id_dari_kepada']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdJenisArsip()
    {
        return $this->hasOne(TblJenisArsip::className(), ['id' => 'id_jenis_arsip']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKlasifikasi0()
    {
        return $this->hasOne(TblKlasifikasi::className(), ['id' => 'klasifikasi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdNamaBerkas()
    {
        return $this->hasOne(TblNamaBerkas::className(), ['id' => 'id_nama_berkas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdKetJra()
    {
        return $this->hasOne(TblRefKetJra::className(), ['id' => 'id_ket_jra']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdNilaiGuna()
    {
        return $this->hasOne(TblRefNilaiGuna::className(), ['id' => 'id_nilai_guna']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSifatSurat()
    {
        return $this->hasOne(TblRefSifatSurat::className(), ['id' => 'id_sifat_surat']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTindakan()
    {
        return $this->hasOne(TblRefTindakan::className(), ['id' => 'id_tindakan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTkPerkembangan()
    {
        return $this->hasOne(TblRefTkPerkembangan::className(), ['id' => 'id_tk_perkembangan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTempatPenyimpanan()
    {
        return $this->hasOne(TblTempatPenyimpanan::className(), ['id' => 'id_tempat_penyimpanan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUnitKerja()
    {
        return $this->hasOne(TblUnitKerja::className(), ['id' => 'id_unit_kerja']);
    }
    
    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->createDate = date('Y-m-d H:i:s');
            $this->userCreate = Yii::$app->user->id;
            $this->userUpdate = Yii::$app->user->id;
        } else {
            $this->updateDate = date('Y-m-d H:i:s');
            $this->userUpdate = Yii::$app->user->id;
        }
        return parent::beforeSave($insert);
    }
    
    public function getUserCreateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userCreate])->one();
        return $user->username;
    }

    public function getUserUpdateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userUpdate])->one();
        return $user->username;
    }
    
    }
