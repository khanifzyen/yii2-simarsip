<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UnitKerja;

/**
 * UnitKerjaSearch represents the model behind the search form about `app\models\UnitKerja`.
 */
class UnitKerjaSearch extends UnitKerja
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'userCreate', 'userUpdate'], 'integer'],
            [['nama_unit_kerja', 'nama_pimpinan', 'createDate', 'updateDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UnitKerja::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'createDate' => $this->createDate,
            'updateDate' => $this->updateDate,
            'userCreate' => $this->userCreate,
            'userUpdate' => $this->userUpdate,
        ]);

        $query->andFilterWhere(['like', 'nama_unit_kerja', $this->nama_unit_kerja])
            ->andFilterWhere(['like', 'nama_pimpinan', $this->nama_pimpinan]);

        return $dataProvider;
    }
}
