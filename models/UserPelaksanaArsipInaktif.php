<?php

namespace app\models;

use Yii;
use app\models\User;


/**
 * This is the model class for table "{{%tbl_user_pelaksana_arsip_inaktif}}".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_pelaksana_arsip_inaktif
 * @property string $createDate
 * @property string $updateDate
 * @property integer $userCreate
 * @property integer $userUpdate
 *
 * @property TblInaktif[] $tblInaktifs
 * @property User $idUser
 * @property TblPelaksanaArsipInaktif $idPelaksanaArsipInaktif
 */
class UserPelaksanaArsipInaktif extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_user_pelaksana_arsip_inaktif}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_pelaksana_arsip_inaktif'], 'required'],
            [['id_user', 'id_pelaksana_arsip_inaktif', 'userCreate', 'userUpdate'], 'integer'],
            [['createDate', 'updateDate'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'id_pelaksana_arsip_inaktif' => Yii::t('app', 'Id Pelaksana Arsip Inaktif'),
            'createDate' => Yii::t('app', 'Create Date'),
            'updateDate' => Yii::t('app', 'Update Date'),
            'userCreate' => Yii::t('app', 'User Create'),
            'userUpdate' => Yii::t('app', 'User Update'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblInaktifs()
    {
        return $this->hasMany(TblInaktif::className(), ['id_user_pelaksana_arsip_inaktif' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPelaksanaArsipInaktif()
    {
        return $this->hasOne(TblPelaksanaArsipInaktif::className(), ['id' => 'id_pelaksana_arsip_inaktif']);
    }
    
    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->createDate = date('Y-m-d H:i:s');
            $this->userCreate = Yii::$app->user->id;
            $this->userUpdate = Yii::$app->user->id;
        } else {
            $this->updateDate = date('Y-m-d H:i:s');
            $this->userUpdate = Yii::$app->user->id;
        }
        return parent::beforeSave($insert);
    }
    
    public function getUserCreateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userCreate])->one();
        return $user->username;
    }

    public function getUserUpdateLabel() {
        $user = User::find()->select('username')->where(['id' => $this->userUpdate])->one();
        return $user->username;
    }
    
    }
